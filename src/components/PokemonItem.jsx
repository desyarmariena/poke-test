import React from 'react'
import {Link} from 'react-router-dom'
import clsx from 'clsx'
import {formatID} from '../utils/pokemon'

export default function PokemonItem({pokemon}) {
  return (
    <Link
      to={`/pokemon/${pokemon.name}`}
      className="bg-gray-300 bg-opacity-40 rounded-md py-6 px-4"
    >
      <article>
        <figure className="relative w-full aspect-square m-auto mb-4">
          <img
            src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${pokemon.id}.svg`}
            alt={pokemon.name}
            loading="lazy"
          />
        </figure>
        <div className="">
          <h2 className="uppercase font-semibold mb-3">
            {pokemon.name}{' '}
            <small className="opacity-80">(#{formatID(pokemon.id)})</small>
          </h2>
          <dl className="text-sm flex ">
            <dt className="inline-block basis-[30%] shrink-0 relative after:absolute after:right-0 after:content-[':']">
              Type
            </dt>
            <dd className="pl-2 flex flex-wrap">
              {pokemon.pokemon_v2_pokemontypes.map((type, index) => (
                <span
                  key={index}
                  className={clsx(
                    'mr-1 last:mr-0 leading-5 px-2 text-xs rounded-md',
                    `badge-${type.pokemon_v2_type.name}`,
                  )}
                >
                  {type.pokemon_v2_type.name}
                </span>
              ))}
            </dd>
          </dl>
          <dl className="text-sm flex ">
            <dt className="inline-block basis-[30%] shrink-0 relative after:absolute after:right-0 after:content-[':']">
              Ability
            </dt>
            <dd className="pl-2 break-words capitalize">
              {pokemon.pokemon_v2_pokemonabilities
                .map(ability => ability.pokemon_v2_ability.name)
                .join(', ')}
            </dd>
          </dl>
        </div>
      </article>
    </Link>
  )
}
