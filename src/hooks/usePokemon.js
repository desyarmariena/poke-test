import {useQuery, useInfiniteQuery} from 'react-query'
import {gql, GraphQLClient} from 'graphql-request'

const client = new GraphQLClient('https://beta.pokeapi.co/graphql/v1beta')

export const useInfinitePokemons = query => {
  return useInfiniteQuery(['pokemons', query], ({pageParam = 0}) =>
    client.request(
      gql`
        query {
          pokemon_v2_pokemon(
            offset: ${pageParam}
            limit: ${query.limit}
            order_by: {id: asc}
          ) {
            name
            id
            pokemon_species_id
            base_experience
            height
            is_default
            order
            pokemon_v2_pokemontypes {
              pokemon_v2_type {
                name
              }
            }
            pokemon_v2_pokemonabilities {
              pokemon_v2_ability {
                name
              }
            }
          }
        }
      `,
    ),
  )
}

export const usePokemonDetail = name => {
  return useQuery(
    ['pokemon', name],
    () =>
      client
        .request(
          gql`
            query getPokemonDetail($name: String!) {
              pokemon_v2_pokemonspecies(where: {name: {_eq: $name}}) {
                id
                name
                pokemon_v2_pokemons {
                  pokemon_v2_pokemonmoves {
                    pokemon_v2_move {
                      id
                      name
                      accuracy
                      power
                    }
                    id
                    level
                    move_id
                    pokemon_id
                  }
                  pokemon_v2_pokemonstats {
                    id
                    base_stat
                    effort
                    stat_id
                    pokemon_v2_stat {
                      name
                      pokemon_v2_characteristics {
                        pokemon_v2_characteristicdescriptions(
                          where: {language_id: {_eq: 9}}
                        ) {
                          description
                          language_id
                        }
                      }
                    }
                  }
                  pokemon_v2_pokemontypes {
                    id
                    pokemon_v2_type {
                      name
                    }
                  }
                  pokemon_v2_pokemonabilities {
                    id
                    pokemon_v2_ability {
                      id
                      name
                    }
                  }
                }
                pokemon_v2_pokemonshape {
                  name
                }
                capture_rate
                pokemon_v2_evolutionchain {
                  id
                  pokemon_v2_pokemonspecies {
                    name
                    id
                    pokemon_v2_pokemons {
                      pokemon_v2_pokemontypes {
                        pokemon_id
                        pokemon_v2_type {
                          name
                        }
                        type_id
                        id
                      }
                    }
                  }
                }
              }
            }
          `,
          {name},
        )
        .then(data => {
          return data.pokemon_v2_pokemonspecies[0]
        }),
    {
      placeholderData: {
        pokemon_v2_evolutionchain: [],
        pokemon_v2_pokemontypes: [],
        pokemon_v2_pokemons: [{}],
      },
    },
  )
}
