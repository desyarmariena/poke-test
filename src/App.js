import React from 'react'
import {Outlet} from 'react-router-dom'
import {BrowserRouter, Routes, Route} from 'react-router-dom'
const PokemonList = React.lazy(() => import('./pages/PokemonList'))
const PokemonDetail = React.lazy(() => import('./pages/PokemonDetail'))

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route
            index
            element={
              <React.Suspense fallback={<>...</>}>
                <PokemonList />
              </React.Suspense>
            }
          />
          <Route
            path="pokemon/:id"
            element={
              <React.Suspense fallback={<>...</>}>
                <PokemonDetail />
              </React.Suspense>
            }
          />
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

function Layout() {
  return (
    <main className="flex flex-col min-h-screen overflow-hidden max-w-4xl m-auto">
      <Outlet />
    </main>
  )
}

export default App
