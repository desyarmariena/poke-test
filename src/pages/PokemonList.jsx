import React, {useState} from 'react'
import PokemonItem from '../components/PokemonItem'
import {useInfinitePokemons} from '../hooks/usePokemon'

export default function PokemonList() {
  const [page, setPage] = useState(0)
  const {
    data: gqlData,
    fetchNextPage,
    isFetching,
    isFetchedAfterMount,
  } = useInfinitePokemons({limit: 20, page: 0})

  return (
    <section className="my-12">
      <h1 className="text-3xl font-bold mb-4">Pokemons</h1>
      <div className="grid grid-cols-[repeat(auto-fit,minmax(200px,1fr))] gap-6">
        {gqlData?.pages?.map((group, i) => (
          <React.Fragment key={i}>
            {group?.pokemon_v2_pokemon?.map(pokemon => {
              return <PokemonItem key={pokemon.id} pokemon={pokemon} />
            })}
          </React.Fragment>
        ))}
      </div>
      {isFetchedAfterMount && (
        <div className="flex justify-center my-4">
          <button
            type="button"
            disabled={isFetching}
            onClick={() => {
              fetchNextPage({pageParam: page + 20})
              setPage(page + 20)
            }}
            className="font-semibold py-1 text-green-900 px-4 border border-green-700 rounded-3xl disabled:border-gray-400 disabled:text-gray-400 disabled:cursor-wait"
          >
            {isFetching ? 'Fetching...' : 'Load more'}
          </button>
        </div>
      )}
    </section>
  )
}
