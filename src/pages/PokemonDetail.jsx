import React from 'react'
import {Link, useParams} from 'react-router-dom'
import {usePokemonDetail} from '../hooks/usePokemon'
import clsx from 'clsx'
import {formatID} from '../utils/pokemon'

const generateColumns = cols => {
  return `grid-cols-${cols}`
}
export default function PokemonDetail() {
  const {id} = useParams()
  const {data, isFetching} = usePokemonDetail(id)
  const {pokemon_v2_evolutionchain: evolutions, pokemon_v2_pokemons = []} = data
  const pokemon = pokemon_v2_pokemons[0] ?? {}

  if (isFetching)
    return (
      <section className="my-12 bg-gray-300 bg-opacity-60 rounded-3xl min-h-[calc(100vh-6rem)] flex justify-center items-center">
        <p className="text-3xl font-bold text-gray-500">Fetching...</p>
      </section>
    )

  return (
    <section className="my-12 bg-gray-300 bg-opacity-60 rounded-3xl min-h-[calc(100vh-6rem)] py-6 px-8 ">
      <div className="flex flex-wrap mb-4">
        <Link to="/">
          <svg
            className="w-[30px] h-[30px] text-gray-600"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path>
          </svg>
        </Link>
        <div>
          <svg
            className="w-6 h-[30px] text-gray-600"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
              clipRule="evenodd"
            ></path>
          </svg>
        </div>
        <h1 className="text-3xl font-bold capitalize">
          {id} <small>(#{formatID(data.id)})</small>
        </h1>
      </div>
      <div className="grid grid-cols-2 gap-x-6">
        <figure className="relative self-center w-full aspect-square max-w-[300px] mb-4">
          <img
            src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${data.id}.svg`}
            alt={data.name}
            loading="lazy"
          />
        </figure>
        <div>
          <h4 className="w-max font-semibold text-lg text-gray-700 mt-4 first:mt-0 bg-[linear-gradient(to_top,salmon_10%,transparent_0)]">
            Types
          </h4>
          <p>
            {data.pokemon_v2_pokemons[0].pokemon_v2_pokemontypes.map(
              ({pokemon_v2_type, id}) => (
                <span
                  key={id}
                  className={clsx(
                    'mr-1 last:mr-0 leading-5 px-2 text-xs rounded-md',
                    `badge-${pokemon_v2_type.name}`,
                  )}
                >
                  {pokemon_v2_type.name}
                </span>
              ),
            )}
          </p>
          <h4 className="w-max font-semibold text-lg text-gray-700 mt-4 first:mt-0 bg-[linear-gradient(to_top,salmon_10%,transparent_0)]">
            Ability
          </h4>
          <p className="text-gray-700">
            {data.pokemon_v2_pokemons[0].pokemon_v2_pokemonabilities
              .map(ability => ability.pokemon_v2_ability.name)
              .join(', ')}
          </p>
          <h4 className="w-max font-semibold text-lg text-gray-700 mt-4 first:mt-0 bg-[linear-gradient(to_top,salmon_10%,transparent_0)]">
            Shape
          </h4>
          <p className="text-gray-700">{data.pokemon_v2_pokemonshape.name}</p>
        </div>
        <article className="my-4 col-start-1 col-span-2">
          <h3 className="font-semibold text-xl text-gray-700 mb-2">
            Evolutions
          </h3>
          <div>
            <ul className="grid grid-cols-[repeat(auto-fit,minmax(150px,1fr))] gap-x-2">
              {evolutions.pokemon_v2_pokemonspecies?.map(species => {
                return (
                  <li key={species.name}>
                    <Link to={`/pokemon/${species.name}`}>
                      <img
                        src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${species.id}.png`}
                        alt={species.name}
                        loading="lazy"
                        className="max-w-[200px] mx-auto"
                      />
                    </Link>
                  </li>
                )
              })}
            </ul>
          </div>
        </article>
        <article className="my-4 col-start-1 col-span-2">
          <h3 className="font-semibold text-xl text-gray-700 mb-2">
            Characteristics
          </h3>
          <div className="border border-gray-600 border-opacity-40 rounded-xl p-2 w-full overflow-x-auto">
            <div
              className={clsx(
                'grid gap-x-2 w-max',
                generateColumns(pokemon.pokemon_v2_pokemonstats.length),
              )}
            >
              {pokemon.pokemon_v2_pokemonstats.map((stat, index) => {
                return (
                  <React.Fragment key={stat.id}>
                    <div className="font-bold border-b border-gray-400 capitalize row-start-1 text-sm">
                      {stat.pokemon_v2_stat.name.replace('-', ' ')}
                    </div>
                    <div className="row-start-2">
                      {stat.pokemon_v2_stat.pokemon_v2_characteristics?.map(
                        (char, index) => {
                          return (
                            <div key={index} className="text-xs py-1">
                              {
                                char.pokemon_v2_characteristicdescriptions[0]
                                  .description
                              }
                            </div>
                          )
                        },
                      )}
                    </div>
                  </React.Fragment>
                )
              })}
            </div>
          </div>
        </article>
        <article className="my-4 col-start-1 col-span-2">
          <h3 className="font-semibold text-xl text-gray-700 mb-2">
            Base Stats
          </h3>
          <div>
            {pokemon.pokemon_v2_pokemonstats.map(stat => {
              return (
                <dl className="flex">
                  <dt className="basis-[140px] relative after:absolute after:right-0 after:content-[':']">
                    {stat.pokemon_v2_stat.name}
                  </dt>
                  <dd className="pl-2">{stat.base_stat}</dd>
                </dl>
              )
            })}
          </div>
        </article>
      </div>
    </section>
  )
}
